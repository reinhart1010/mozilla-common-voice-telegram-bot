declare module 'country-flag-emoji' {
    function get(country: string): CountryEmoji;

    interface CountryEmoji {
        code: string,
        unicode: string,
        name: string,
        emoji: string
    }
}