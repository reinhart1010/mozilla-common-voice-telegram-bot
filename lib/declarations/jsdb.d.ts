declare module '@small-tech/jsdb' {
    export default class JSDB {
        constructor(basePath: string, options?: JSDBOptions): Proxy<Map<string, JSTable>>;
        dataProxy: Proxy<Map<string, JSTable>>;
        getHandler(target: Object, property: string, receiver?: Object): Boolean;
        isBeingInstantiatedByFactoryMethod: Boolean;
        loadTables(): void;
        loadingTables: Boolean;
        static open(basePath: string, options?: JSDBOptions): Proxy<Map<string, JSTable>>;
        openDatabases: Object<string, JSDB>;
        onTableDelete: Function<void>;
        proxyHandler(): ProxyHandlerResult;
        setHandler(target: Object, property: string, value: any, receiver?: Object): Boolean;
        tableDataProxies: Array<JSTable>;
        tableNames: Array<string>;
    }

    class JSDBOptions {
        deleteIfExists: Boolean;
    }
    
    class JSDF {
        serialize(value: any, key: string|number, parentType: string): string;
    }
    
    class JSTable {
        _create(tablePath?: string): void;
        close: () => Promise<void>;
        compact(): void;
        create(): void;
        delete(): Promise<void>;
        load(): void;
        persisChange(change: Object): void;
    }
    
    class ProxyHandlerResult {
        get(): Boolean; // Reflect.get...
        set(): Boolean; // Reflect.set...
    }
};
