import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { LanguageNotFoundError } from '../exceptions.js';
import StorageHandler from '../handlers/storageHandler.js';

export default function setlanguage(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('setlanguage', async (ctx) => {
        const split: Array<string> = ctx.message.text.split(' ');
        if (split.length < 2) {
            await ctx.replyWithHTML('To set a language, use /setlanguage <pre>{{language}}</pre>');
            return;
        }
        try {
            await storageHandler.setUserPreferredLanguage(ctx.message.from.id, split[1]);
            await ctx.reply('Done!');
        } catch (e) {
            if (e instanceof LanguageNotFoundError) {
                await ctx.reply('Well, this language doesn\'t seem to be valid yet');
            } else throw e;
        }
    });
}