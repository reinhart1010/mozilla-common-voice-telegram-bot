import { escape } from 'html-escaper';
import { Context, Telegraf } from 'telegraf';
import { InputFile, Message, Update } from 'telegraf/typings/core/types/typegram';
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types.js';
import StorageHandler from '../handlers/storageHandler.js';

enum ListenAction {
    NO,
    // REPORT,
    SKIP,
    YES,
}

class TextMessageTemplate {
    constructor(ctx: Context<Update>, message: string, extra: ExtraReplyMessage | undefined) {
        this.ctx = ctx;
        this.extra = extra;
        this.message = message;
    };
    ctx: Context<Update>;
    extra: ExtraReplyMessage | undefined;
    message: string;
    sendAsHTML: () => Promise<Message.TextMessage> = () => this.ctx.replyWithHTML(this.message, this.extra);
    sendAsMarkdown: () => Promise<Message.TextMessage> = () => this.ctx.replyWithMarkdown(this.message, this.extra);
    sendAsMarkdownV2: () => Promise<Message.TextMessage> = () => this.ctx.replyWithMarkdownV2(this.message, this.extra);
    sendAsPlain: () => Promise<Message.TextMessage> = () => this.ctx.reply(this.message, this.extra);
}

export async function handleListenAction(bot: Telegraf<Context<Update>>, ctx: Context<Update>, storageHandler: StorageHandler, action: ListenAction){
    let clipCheck: Clip | null = await storageHandler.getClipForUser(ctx, false);
    clipCheck = JSON.parse(JSON.stringify(clipCheck));
    if (clipCheck == null) {
        await ctx.reply('You haven\'t /listen to any clips yet.');
        return;
    }
    switch (action) {
        case ListenAction.NO:
            await storageHandler.validateClip(clipCheck, bot.telegram, false);
            await ctx.reply('Thanks for your contribution!');
            break;
        case ListenAction.SKIP:
            await storageHandler.updateClipMessageIds(clipCheck, bot.telegram, null, null);
            break;
        case ListenAction.YES:
            await storageHandler.validateClip(clipCheck, bot.telegram, true);
            await ctx.reply('Thanks for your contribution!');
            break;
    }
}

export default function listen(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('listen', async (ctx) => {
        const clip: Clip | null = await storageHandler.getClipForUser(ctx);
        if (clip == null) {
            await ctx.reply('Well, it seems that there are no clips to validate yet. That said, you can still /speak or /changelanguage!');
            return;
        }
        await ctx.replyWithChatAction('upload_voice');
        const voiceMessage: Message.VoiceMessage = await ctx.replyWithVoice({source: await storageHandler.convertClipForTelegram(clip)});
        const hintMessage: Message.TextMessage = await renderListenMessage(ctx, storageHandler, clip, voiceMessage).sendAsHTML();
        await storageHandler.updateClipMessageIds(clip, ctx.telegram, voiceMessage, hintMessage);
    });

    bot.action('LISTEN:NO', (ctx) => handleListenAction(bot, ctx, storageHandler, ListenAction.NO));
    // bot.action('LISTEN:REPORT', (ctx) => handleListenAction(bot, ctx, storageHandler, ListenAction.REPORT));
    bot.action('LISTEN:SKIP', (ctx) => handleListenAction(bot, ctx, storageHandler, ListenAction.SKIP));
    bot.action('LISTEN:YES', (ctx) => handleListenAction(bot, ctx, storageHandler, ListenAction.YES));
}

export function renderListenMessage(ctx: Context<Update>, storageHandler: StorageHandler, clip: Clip, voiceMessage: Message.VoiceMessage): TextMessageTemplate {
    return new TextMessageTemplate(ctx, `Okay. So, is this the correct way to pronounce:\n\n<pre>${escape(clip.sentence.text)}</pre>?`, {
        reply_markup: {
            inline_keyboard: [
                [
                    {
                        text: '👍 Yes',
                        callback_data: `LISTEN:YES`
                    },
                    {
                        text: '👎 No',
                        callback_data: `LISTEN:NO`
                    }
                ],
                [
                    {
                        text: 'ℹ️ Contribution Criteria',
                        url: `${storageHandler.requestHandler.baseUrl}/${clip.language}/criteria`
                    }
                ],
                [
                    {
                        text: '🙅 Skip',
                        callback_data: `LISTEN:SKIP`
                    },
                    // {
                    //     text: '🚩 Report',
                    //     callback_data: `LISTEN:REPORT`
                    // }
                ]
            ]
        },
        reply_to_message_id: voiceMessage.message_id
    });
}
