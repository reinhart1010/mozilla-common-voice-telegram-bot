import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler.js';

export default function refreshlanguages(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('refreshlanguages', async (ctx) => {
        await storageHandler.refreshLanguages();
        await ctx.reply('Done!');
    });
}