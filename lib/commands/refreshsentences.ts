import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler.js';

export default function setlanguage(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('refreshsentences', async (ctx) => {
        await storageHandler.refreshSentences();
        await ctx.reply('Done!');
    });
}
