import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler';

export default function start(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.start(async (ctx) => {
        await ctx.reply('Hi, I\'m Caps, Captain of Automation and Personality Subsystem of Shift!');
        await ctx.reply('This is Shift\'s (aka. @reinhart1010) version of Mozilla Common Voice bot for Telegram, as shared in https://gitlab.com/reinhart1010/mozilla-common-voice-bot.');
        await ctx.replyWithHTML(`That said, I'm functional and currently serving requests directly to <b>${storageHandler.requestHandler.baseUrl}</b>. Please note that your contributions are subject to Common Voice <a href="https://commonvoice.mozilla.org/en/terms">Terms of Use</a> and <a href="https://commonvoice.mozilla.org/en/privacy">Privacy Policy</a>.`);
        await ctx.reply('Alright, if you don\'t have any other questions, just /speak or /listen to sentences to help contribute a better voice dataset together. You may /cancel anytime, or /switchlanguage anytime, but please note that I\'ve not been programmed to speak other than English yet.');
    });
}