import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler.js';

export default function refreshclips(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('refreshclips', async (ctx) => {
        await storageHandler.refreshClips();
        await ctx.reply('Done!');
    });
}