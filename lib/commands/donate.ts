import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler.js';

export default function donate(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('donate', async (ctx) => {
        await ctx.reply('Yeah you can support us by sending donations to:', {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'GitHub Sponsors',
                            url: 'https://github.com/reinhart1010',
                        },
                    ],
                    [
                        {
                            text: 'PayPal',
                            url: 'https://paypal.me/reinhart1010',
                        }
                    ]
                ]
            }
        });
        await ctx.reply('And if you live on Indonesia, you can also support us directly via:', {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'Karyakarsa',
                            url: 'https://karyakarsa.com/reinhart1010',
                        }
                    ],
                    [
                        {
                            text: 'Saweria',
                            url: 'https://saweria.co/reinhart1010',
                        },
                    ],
                    [
                        {
                            text: 'Trakteer',
                            url: 'https://trakteer.id/reinhart1010',
                        }
                    ]
                ]
            }
        });
        await ctx.reply('Well, that\'s it, I guess. Hopefully we can finally move this server to something better and faster!')
    });
}
