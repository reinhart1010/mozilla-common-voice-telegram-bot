import { Context, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import StorageHandler from '../handlers/storageHandler.js';

export default function help(bot: Telegraf<Context<Update>>, storageHandler: StorageHandler) {
    bot.command('help', async (ctx) => {
        await ctx.reply('To contribute, you can /speak or /listen to sentences made by thousands of Common Voice contributors.\nYou can also /donate to us to keep this bot healthy.');
    });
}