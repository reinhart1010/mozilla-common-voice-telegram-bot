interface Language {
    id: number;
    name: string;
    target_sentence_count: number;
    native_name: string;
    is_contributable: number;
    is_translated: number;
    text_direction: string;
}