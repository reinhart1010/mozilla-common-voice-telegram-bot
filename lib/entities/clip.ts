interface Clip {
    assignedTo?: number;
    assignedHintMessage?: number;
    assignedVoiceMessage?: number;
    audioSrc: URL;
    expiredAt: Date;
    glob: string;
    id: number;
    language: string;
    sentence: Sentence;
}
