interface Sentence {
    assignedTo?: number;
    id: string;
    language: string;
    text: string;
}
