import { Context, Markup, Telegraf } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { SceneContextScene, WizardContext, WizardContextWizard, WizardScene, WizardSessionData } from 'telegraf/typings/scenes';
import RequestHandler from '../handlers/requestHandler';
import StorageHandler from '../handlers/storageHandler';

export default function speakWizard (storageHandler: StorageHandler): WizardScene<any> {
    return new WizardScene(
        'speakWizard',
        async (ctx) => {
            
            await ctx.reply(
              'Step 1',
              Markup.inlineKeyboard([
                Markup.button.url('❤️', 'http://telegraf.js.org'),
                Markup.button.callback('➡️ Next', 'next'),
              ])
            )
            return ctx.wizard.next()
          },
          // stepHandler,
          async (ctx) => {
            await ctx.reply('Step 3')
            return ctx.wizard.next()
          },
          async (ctx) => {
            await ctx.reply('Step 4')
            return ctx.wizard.next()
          },
          async (ctx) => {
            await ctx.reply('Done')
            return await ctx.scene.leave()
          }
    );
}