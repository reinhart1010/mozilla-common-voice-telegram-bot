import * as dotenv from 'dotenv'
import * as fs from 'fs';
import { Telegraf } from 'telegraf';
import StorageHandler from './handlers/storageHandler.js';

dotenv.config()

const storageHandler: StorageHandler = new StorageHandler();
const bot: Telegraf = new Telegraf(process.env.TG_TOKEN!);

// Initialize commands
console.info('Loading commands...');
fs.readdir('lib/commands', (err: NodeJS.ErrnoException | null, files: Array<string>) => {
    if (err) {
      console.error(err);
    }
    files.forEach((file) => {
        if (file.endsWith('.js')) {
            console.info(`   🦾    ❨Core❩ Loading ${file}`);
            import(`./commands/${file}`).then((module) => {
                module.default(bot, storageHandler);
            });
        }
    });
});

// Fetch all clips
storageHandler.refreshLanguages().then(async () => {
    await storageHandler.clearExpiredClips();
    await storageHandler.refreshClips();
    await storageHandler.refreshSentences();
});

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
bot.launch();
