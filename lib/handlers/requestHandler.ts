import axios, { AxiosInstance } from 'axios';
import bent from 'bent';
import crypto from 'crypto';
import * as uuid from 'uuid';
import { AuthenticationPair } from './storageHandler';

export interface RequestHandlerParams {
    baseUrl?: string
}

export default class RequestHandler {
    private _get: bent.RequestFunction<bent.ValidResponse>;
    private _post: bent.RequestFunction<bent.ValidResponse>;
    private _put: bent.RequestFunction<bent.ValidResponse>;
    private _axios: AxiosInstance;
    
    baseUrl: string = 'https://commonvoice.mozilla.org';

    constructor(params? : RequestHandlerParams) {
        if (params != null) {
            if (params.baseUrl != null) this.baseUrl = params.baseUrl;
        }
        this._get = bent(this.baseUrl, 'json', 'GET', 200, {'User-Agent': 'CapsCommonVoiceBot/1.0 (+https://reinhart1010.id)'});
        this._post = bent(this.baseUrl, 'json', 'POST', 200, {'User-Agent': 'CapsCommonVoiceBot/1.0 (+https://reinhart1010.id)'});
        this._put = bent(this.baseUrl, 'json', 'PUT', 200, {'User-Agent': 'CapsCommonVoiceBot/1.0 (+https://reinhart1010.id)'});
        this._axios = axios.create({url: this.baseUrl, headers: {'User-Agent': 'CapsCommonVoiceBot/1.0 (+https://reinhart1010.id)'}});
    }

    /**
     * @ImportedFromCommonVoice
     */ 
     dec2hex(n: number) {
        return ('0' + n.toString(16)).substring(-2);
    }

    async getClips(language: string = 'en', count: number = 10): Promise<Array<Clip>> {
        try {
            const res: Array<Map<string, any>> = await this._get(`/api/v1/${language}/clips?count=${count}`) as Array<Map<string, any>>;
            const expiredAt: Date = new Date();
            expiredAt.setTime(expiredAt.getTime() + (43200));
            return res.map((el: any) => {
                el['language'] = language;
                el['expiredAt'] = expiredAt;
                return el as unknown as Clip;
            });
        } catch (e) {
            console.error(e);
            return [];
        }
    }

    async getLanguages (contributableOnly: Boolean = true): Promise<Array<Language>> {
        try {
            const res: Array<Object> = await this._get(`/api/v1/languages`) as Array<Object>;
            let languages: Array<Language> = res.map((el) => el as Language);
            if (contributableOnly) languages = languages.filter((lang) => lang.is_contributable > 0);
            return languages;
        } catch (e) {
            console.error(e);
            return [];
        }
    }

    async getSentences(language: string = 'en', count: number = 10): Promise<Array<Sentence>> {
        try {
            const res: Array<Map<string, any>> = await this._get(`/api/v1/${language}/sentences?count=${count}`) as Array<Map<string, any>>;
            return res.map((el: any) => {
                el['language'] = language;
                return el as unknown as Sentence;
            });
        } catch (e) {
            console.error(e);
            return [];
        }
    }

    generateGUID (userId: number): string {
        return uuid.v5(`telegram:${userId}`, process.env.CV_UUID_NAMESPACE!);
    }

    /**
     * @ImportedFromCommonVoice
     */ 
    generateAuthToken(length: number = 40) {
        const arr = new Uint8Array(length / 2);
        crypto.getRandomValues(arr);
        return Array.from(arr, this.dec2hex).join('');
    }

    async submitClip (clipData: Buffer, language: string = 'en'): Promise<FilePrefix|null> {
        try {
            return await this._post(`/api/v1/${language}/clips`, clipData) as FilePrefix;
        } catch (e) {
            return null;
        }
    }

    async validateClip (clipId: number, isValid: Boolean, language: string = 'en', authToken: AuthenticationPair): Promise<SubmitClipGlob|null> {
        try {
            console.log(`/api/v1/${language}/clips/${clipId}/votes`);
            const response = await this._axios.post(`${this.baseUrl}/api/v1/${language}/clips/${clipId}/votes`, {'isValid': isValid}, {
                headers: {
                    // 'Authorization': authToken.toAuthorizationHeaderString(),
                    'Authorization': process.env.CV_AUTH_TOKEN!,
                }
            });
            return response.data as SubmitClipGlob;
        } catch (e) {
            console.error(e);
            return null;
        }
    }
}
