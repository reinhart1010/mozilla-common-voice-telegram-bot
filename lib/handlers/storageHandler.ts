import bent from 'bent';
import countryFlagEmoji from 'country-flag-emoji';
import ffmpeg from 'fluent-ffmpeg';
import { path as ffmpegPath } from '@ffmpeg-installer/ffmpeg';
import { path as ffprobePath } from '@ffprobe-installer/ffprobe';
import * as fs from 'fs/promises';
import JSDB from '@small-tech/jsdb';
import path from 'path';
import UserAgent from 'user-agents';

import RequestHandler, { RequestHandlerParams } from './requestHandler.js';
import { ClipNotFoundError, LanguageNotFoundError } from '../exceptions.js';
import { Context, Telegram } from 'telegraf';
import { Message, Update } from 'telegraf/typings/core/types/typegram.js';

export const MAX_CLIP_THRESHOLD: number = 50;
export const MAX_SENTENCE_THRESHOLD: number = 25;

ffmpeg.setFfmpegPath(ffmpegPath);
ffmpeg.setFfprobePath(ffprobePath);
console.info(`   ✅    ❨StorageHandler❩ ffmpeg installed on ${ffmpegPath}`);
console.info(`   ✅    ❨StorageHandler❩ ffprobe installed on ${ffprobePath}`);

export class AuthenticationPair {
    constructor(guid: string, authToken: string) {
        this.guid = guid;
        this.authToken = authToken
    }
    guid: string;
    authToken: string;
    toAuthorizationHeaderString(): string {
        return `Basic ${Buffer.from(`${this.guid}:${this.authToken}`).toString('base64')}`;
    }
}

export interface StorageHandlerParams {
    requestHandlerParams?: RequestHandlerParams;
}

export default class StorageHandler {
    jsdb = JSDB.open('db');
    requestHandler: RequestHandler;

    constructor(params?: StorageHandlerParams) {
        this.requestHandler = new RequestHandler(params?.requestHandlerParams);
    }

    async clearExpiredClips(): Promise<void> {
        if (!this.jsdb.clips || this.jsdb.clips.length == 0) return;
        const clips: Array<Clip> = (this.jsdb.clips as Array<Clip>).filter((el: Clip) => {
            return (Date.parse(el.expiredAt.toJSON()) > new Date().getTime() || el.assignedTo != null);
        });
        await this.jsdb.clips.delete();
        this.jsdb.clips = clips;
    }

    async convertClipForTelegram(clip: Clip): Promise<Buffer> {
        console.log(`   🎤    ❨Convert❩  Incoming clip ${clip.id}`);
        const mp3Path: string = `clips/incoming/${clip.id}.mp3`;
        const oggPath: string = `clips/incoming/${clip.id}.ogg`;

        try {
            return await fs.readFile(oggPath);
        } catch (e) {
            try {
                await fs.readFile(mp3Path);
                console.info(`   💾    ❨Convert❩  ╰─ ${oggPath} already saved`);
            } catch(f) {
                const raw: Buffer = await (bent('buffer', {
                    'User-Agent': new UserAgent().toString()
                }))(clip.audioSrc.toString()) as Buffer;
                console.info(`   💾    ❨Convert❩  ╰─ Downloading MP3 file`);
                await fs.writeFile(mp3Path, raw);
                console.info(`   💾    ❨Convert❩  ╰─ Saved to ${mp3Path} with size ${raw.length}`);
            } finally {
                console.info(`   💾    ❨Convert❩  ╰─ Attempting to convert to ${oggPath}`);
                return await new Promise((resolve, reject) => {
                    ffmpeg().input(mp3Path)
                    .fromFormat('mp3')
                    .toFormat('ogg')
                    .audioCodec('libopus')
                    .save(oggPath)
                    .on('end', async () => {
                        resolve(await fs.readFile(oggPath));
                    });
                });
            }
        }
    }

    generateAuthorizationToken (userId: number): AuthenticationPair {
        if (!this.jsdb.users) this.jsdb.users = {};
        if (!this.jsdb.users[userId]) this.jsdb.users[userId] = {};
        if (!this.jsdb.users[userId].authToken) this.jsdb.users[userId].authToken = this.requestHandler.generateAuthToken();
        if (!this.jsdb.users[userId].guid) this.jsdb.users[userId].guid = this.requestHandler.generateGUID(userId);
        
        return new AuthenticationPair(this.jsdb.users[userId].guid, this.jsdb.users[userId].authToken);
    }

    async getClipForUser(ctx: Context<Update>, withOtherFreeClips: boolean = true): Promise<Clip|null> {
        if (!this.jsdb.clips || this.jsdb.clips.length == 0) await this.refreshClips();
        const userId: number = (ctx.callbackQuery ? ctx.callbackQuery! : ctx.message!).from.id;
        const userAssignedClips: Array<Clip> = this.jsdb.clips.where('assignedTo').is(userId).get();
        const otherClips: Array<Clip> = this.jsdb.clips.where('language').is(this.getUserPreferredLanguage(userId)).get().filter((clip: Clip) => clip.assignedTo == null);

        if (userAssignedClips.length > 0) return userAssignedClips[0];
        if (!withOtherFreeClips || otherClips.length == 0) return null;

        const selectedClip: Clip = otherClips[Math.floor(Math.random() * otherClips.length)];
        const selectedClipIndex: number = this.jsdb.clips.findIndex((clip: Clip) => clip.id == selectedClip.id);
        this.jsdb.clips[selectedClipIndex].assignedTo = userId;
        selectedClip.assignedTo = userId;
        return selectedClip;
    }

    getUserPreferredLanguage(userId: number): string {
        if (!this.jsdb.users) this.jsdb.users = {};
        if (!this.jsdb.users[userId]) this.jsdb.users[userId] = {};
        return this.jsdb.users[userId].preferredLanguage || 'en';
    }

    async refreshClips(): Promise<void> {
        if (!this.jsdb.languages || this.jsdb.languages.length == 0) await this.refreshLanguages();
        await this.clearExpiredClips();

        if (!this.jsdb.clips) this.jsdb.clips = [];
        const languages: Array<Language> = this.jsdb.languages as Array<Language>;
        const clips: Array<Clip> = (this.jsdb.clips as Array<Clip>).filter((el) => el.assignedTo == null);

        console.info(`There are currently ${clips.length} unlocked clips served.`);

        // Check how many clips have been requested 
        const counter: Map<string, number> = new Map<string, number>();
        clips.forEach((clip: Clip) => {
            if (counter.has(clip.language)) counter.set(clip.language, 0);
            counter.set(clip.language, counter.get(clip.language)!);
        });
        languages.forEach((language: Language) => {
            if (!counter.has(language.name)){
                console.info(`   ${countryFlagEmoji.get(language.name) != null ? countryFlagEmoji.get(language.name).emoji : '🏁\uFE0F'}    Got new language: ${language.name}`);
                counter.set(language.name, 0);
            }
        })

        // Iterate over available sentences
        const promiseQueue: Array<Promise<Array<Clip>>> = [];
        counter.forEach(async (value: number, language: string) => {
            const currentCount: number = counter.get(language)!;
            // console.info(`Language ${language}: ${currentCount}`);
            promiseQueue.push(this.requestHandler.getClips(language, MAX_CLIP_THRESHOLD - currentCount));
        });

        const newClips: Array<Clip> = (await Promise.all(promiseQueue)).flat();
        newClips.forEach((clip: Clip) => {
            this.jsdb.clips.push(clip);
        });
    }

    async refreshLanguages(): Promise<void> {
        if (this.jsdb.languages) await this.jsdb.languages.delete();
        this.jsdb.languages = await this.requestHandler.getLanguages();
    }

    async refreshSentences(): Promise<void> {
        if (!this.jsdb.languages || this.jsdb.languages.length == 0) await this.refreshLanguages();
        if (!this.jsdb.sentences) this.jsdb.sentences = [];
        const languages: Array<Language> = this.jsdb.languages as Array<Language>;
        const sentences: Array<Sentence> = this.jsdb.sentences as Array<Sentence>;
        
        console.info(`There are currently ${sentences.length} unlocked sentences served.`);
        if (sentences.length > 1.5 * languages.length * MAX_SENTENCE_THRESHOLD) return;

        // Check how many sentences have been requested 
        const counter: Map<string, number> = new Map<string, number>();
        sentences.forEach((sentence: Sentence) => {
            if (counter.has(sentence.language)) counter.set(sentence.language, 0);
            counter.set(sentence.language, counter.get(sentence.language)!);
        });
        languages.forEach((language: Language) => {
            if (!counter.has(language.name)){
                console.info(`Got new language: ${language.name}`);
                counter.set(language.name, 0);
            }
        })

        // Iterate over available sentences
        const promiseQueue: Array<Promise<Array<Sentence>>> = [];
        counter.forEach(async (value: number, language: string) => {
            const currentCount: number = counter.get(language)!;
            // console.info(`Language ${language}: ${currentCount}`);
            promiseQueue.push(this.requestHandler.getSentences(language, MAX_SENTENCE_THRESHOLD - currentCount));
        });

        const newSentences: Sentence[] = (await Promise.all(promiseQueue)).flat();
        newSentences.forEach((sentence: Sentence) => {
            this.jsdb.sentences.push(sentence);
        });
    }

    async setUserPreferredLanguage(userId: number, preferredLanguage: string) {
        const languageCheck: Array<Language> = this.jsdb.languages.filter((el: Language) => el.name == preferredLanguage || el.native_name == preferredLanguage) as Array<Language>;
        if (!this.jsdb.languages || languageCheck.length < 1) throw new LanguageNotFoundError();
        if (!this.jsdb.users) this.jsdb.users = {};
        if (!this.jsdb.users[userId]) this.jsdb.users[userId] = {};
        this.jsdb.users[userId].preferredLanguage = languageCheck[0].name;
    }

    async updateClipMessageIds(clip: Clip, telegram: Telegram, voiceMessage: Message.VoiceMessage|null, hintMessage: Message.TextMessage|null) {
        if (!this.jsdb.clips || this.jsdb.clips.length == 0) return;

        const clipIndex = this.jsdb.clips.findIndex((el: Clip) => el.id == clip.id);

        if (clipIndex < 0) throw new ClipNotFoundError();
        clip = this.jsdb.clips[clipIndex];

        // Remove old messages (if possible) and reassign user and chat IDs
        if (clip.assignedTo){
            if (clip.assignedHintMessage) telegram.deleteMessage(clip.assignedTo, clip.assignedHintMessage);
            if (clip.assignedVoiceMessage) telegram.deleteMessage(clip.assignedTo, clip.assignedVoiceMessage);
        }
        clip.assignedTo = voiceMessage?.chat.id;
        clip.assignedVoiceMessage = voiceMessage?.message_id;
        clip.assignedHintMessage = hintMessage?.message_id;
    }

    async validateClip(selectedClip: Clip, telegram: Telegram, isValid: boolean): Promise<void> {
        await this.requestHandler.validateClip(selectedClip.id, isValid, selectedClip.language, this.generateAuthorizationToken(selectedClip.assignedTo!));
        const selectedClipIndex: number = this.jsdb.clips.findIndex((clip: Clip) => clip.id == selectedClip.id);
        this.jsdb.clips.splice(selectedClipIndex, 1);

        // Remove old messages (if possible) and reassign user and chat IDs
        if (selectedClip.assignedTo) {
            if (selectedClip.assignedHintMessage) telegram.deleteMessage(selectedClip.assignedTo, selectedClip.assignedHintMessage);
            if (selectedClip.assignedVoiceMessage) telegram.deleteMessage(selectedClip.assignedTo, selectedClip.assignedVoiceMessage);
        }
        // Delete the files, too
        try {
            fs.unlink(`clips/incoming/${selectedClip.id}.ogg`);
            fs.unlink(`clips/incoming/${selectedClip.id}.mp3`);
        } catch (pass) {}
    }
}
