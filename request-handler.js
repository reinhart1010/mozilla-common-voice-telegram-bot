"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bent_1 = __importDefault(require("bent"));
class RequestHandler {
    constructor(params) {
        this.baseUrl = "https://commonvoice.mozilla.org";
        this.getClips = () => __awaiter(this, void 0, void 0, function* () {
            const res = yield this._get("/api/v1/en/clips?count=10");
            console.log(res);
        });
        if (params != null) {
            if (params.baseUrl != null)
                this.baseUrl = params.baseUrl;
        }
        this._get = (0, bent_1.default)(this.baseUrl, "json", "GET", 200);
        this._post = (0, bent_1.default)(this.baseUrl, "json", "POST", 200);
        this._put = (0, bent_1.default)(this.baseUrl, "json", "PUT", 200);
    }
}
exports.default = RequestHandler;
